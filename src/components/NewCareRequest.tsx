import React, {ChangeEvent} from "react";
import {Button, Modal} from "react-bootstrap";
import {CareRequest} from "../interfaces/CareRequest.interface";
import uuid from "uuid";

export interface NewCareRequestProps {
    onSubmit: Function;
}

interface newCareRequestState {
    showModal: boolean;
    newCareRequest: CareRequest;
}

export class NewCareRequest extends React.Component<NewCareRequestProps, newCareRequestState> {
    constructor(props: NewCareRequestProps) {
        super(props);
        this.state = {
            showModal: false,
            newCareRequest: {
                uuid: uuid(),
                clientName: '',
                description: '',
                endDateTime: new Date(),
                startDateTime: new Date(),
                type: "household",
                state: 'open'
            }
        }



        this.closeModal = this.closeModal.bind(this);
        this.openModal = this.openModal.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    clearState() {
        this.setState( {
            showModal: false,
            newCareRequest: {
                uuid: uuid(),
                clientName: '',
                description: '',
                endDateTime: new Date(),
                startDateTime: new Date(),
                type: "household",
                state: 'open'
            }
        }
        )
    }

    closeModal() {
        this.clearState()
    }

    openModal() {
        this.setState({
            showModal: true,
        })
    }

    onSubmit() {
        this.closeModal();
        this.props.onSubmit(this.state.newCareRequest);
    }

    handleChange(event: ChangeEvent<HTMLInputElement> | ChangeEvent<HTMLSelectElement>) {
        let state = this.state;
        const name = event.target.name;

        switch (name) {
            case 'clientName':
                state.newCareRequest.clientName = event.target.value;
                break;
            case 'description':
                state.newCareRequest.description = event.target.value;
                break;
            case 'type':
                // should refactor type (and state) to enum. But i'm out of time now
                if(event.target.value === 'household' || event.target.value === 'medical') {
                    state.newCareRequest.type = event.target.value;
                }
                break
            default:
                console.error('Unknown input field' + event.target.name)
                break;

        }

        this.setState(state)
    }

    handleSubmit() {

    }

    render() {

        return (
            <>
                <Button variant="primary" onClick={this.openModal}>
                    New Request
                </Button>

                <Modal show={this.state.showModal} onHide={this.closeModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>New Care Request</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>New Care Request
                        <form onSubmit={this.handleSubmit}>
                            <label>
                                client name:
                                <input
                                    name="clientName"
                                    type="text"
                                    value={this.state.newCareRequest.clientName}
                                    onChange={this.handleChange}
                                />
                            </label>
                            <label>
                                description:
                                <input
                                    name="description"
                                    type="textarea"
                                    value={this.state.newCareRequest.description}
                                    onChange={this.handleChange}
                                />
                            </label>
                            <label>
                                Type:
                                <select
                                    name="type"
                                    value={this.state.newCareRequest.type}
                                    onChange={this.handleChange}
                                >
                                    <option value="household">Household</option>
                                    <option value="medical">Medical</option>

                                </select>
                            </label>
                            {/*<label>*/}
                            {/*    start Date*/}
                            {/*    <input*/}
                            {/*        name="startDateTime"*/}
                            {/*        type="datetime-local"*/}
                            {/*        value={this.state.newCareRequest.startDateTime.}*/}
                            {/*        onChange={this.handleChange}*/}
                            {/*    />*/}
                            {/*</label>*/}
                        </form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.closeModal}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={this.onSubmit}>
                            Save
                        </Button>
                    </Modal.Footer>
                </Modal>
            </>
        )

    }


}