import React from "react";
import { Container} from "react-bootstrap";
import {CareRequest} from "../interfaces/CareRequest.interface";
import {NewCareRequest} from "./NewCareRequest";
import { v4 as uuid } from 'uuid';
import {ListCareRequests} from "./ListCareRequests";

interface marketplaceState {
    careRequests: CareRequest[]
}

export class Marketplace extends React.Component<any, marketplaceState> {
    constructor(props: any) {
        super(props);
        this.state = {
            careRequests: [
                {
                    uuid: uuid(),
                    clientName: "New Client",
                    description: "New Care Request",
                    endDateTime: new Date(),
                    startDateTime: new Date(),
                    state: "open",
                    type: "household"
                }
            ]
        }

        this.createNewRequest = this.createNewRequest.bind(this);

    }

    createNewRequest(careRequest: CareRequest) {

        // using hardcoded data for now. I didnt have time to learn react forms yet
        // const newRequest: CareRequest = {
        //     uuid: uuid(),
        //     clientName: "New Client",
        //     description: "New Care Request",
        //     endDateTime: new Date(),
        //     startDateTime: new Date(),
        //     state: "open",
        //     type: "household"
        // }

        const currentState = this.state;
        const updatedCareRequests = currentState.careRequests.concat(careRequest);
        this.setState({
            careRequests: updatedCareRequests
        })
    }


    render() {
        return (
            <Container>
                <h1>Mini marktplace</h1>

                <NewCareRequest onSubmit={(event: CareRequest) => this.createNewRequest(event)}/>
                <div className="mt-3">
                    <ListCareRequests careRequests={this.state.careRequests} ></ListCareRequests>

                </div>
            </Container>
        )
    }


}