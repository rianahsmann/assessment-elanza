import {CareRequest} from "../interfaces/CareRequest.interface";
import React from "react";
import {Button, Modal, Table} from "react-bootstrap";

export interface ListCareRequestsProps {
    careRequests: CareRequest[]
}

interface ListCareRequestsState {
    modal: {
        careRequest: CareRequest | undefined
    };

}

export class ListCareRequests extends React.Component<ListCareRequestsProps, ListCareRequestsState> {
    constructor(props: ListCareRequestsProps) {
        super(props);
        this.state = {
            modal: {
                careRequest: undefined
            }
        }

        this.showDetails = this.showDetails.bind(this)
        this.closeModal = this.closeModal.bind(this)
    }

    showDetails(careRequest: CareRequest) {
        this.setState({
            modal: {careRequest: careRequest}
        })
    }

    closeModal() {
        this.setState({
            modal: {careRequest: undefined}
        })
    }

    apply(careRequest: CareRequest) {
        // eslint-disable-next-line no-restricted-globals
        const confirmed = confirm("Are you sure you apply for this CareRequest?")

        if (confirmed) {
            careRequest.state = 'closed'
            this.closeModal()
        }
    }

    render() {
        const careRequests = this.props.careRequests;
        const careRequestRows = careRequests.map((item: CareRequest) =>
            <tr key={item.uuid} onClick={() => this.showDetails(item)}>
                <td>{item.type}</td>
                <td>{item.state}</td>
                <td>{item.startDateTime.getDate()} - {item.startDateTime.getMonth() + 1}</td>
            </tr>
        )
        const detailRequest = this.state.modal.careRequest;
        let modal: any;
        if (!!detailRequest) {
            modal = <Modal show={!!detailRequest} onHide={this.closeModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Care Request Details</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Table>
                        <tbody>
                        <tr>
                            <td>Client</td>
                            <td>{detailRequest.clientName}</td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>{detailRequest.type}</td>
                        </tr>
                        <tr>
                            <td>State</td>
                            <td>{detailRequest.state}</td>
                        </tr>
                        <tr>
                            <td>Start Date</td>
                            <td>{detailRequest.startDateTime.getDate()} - {detailRequest.startDateTime.getMonth() + 1}</td>
                        </tr>
                        <tr>
                            <td>End Date</td>
                            <td>{detailRequest.endDateTime.getDate()} - {detailRequest.endDateTime.getMonth() + 1}</td>
                        </tr>
                        <tr>
                            <td>Description</td>
                            <td>{detailRequest.description}</td>
                        </tr>
                        </tbody>
                    </Table>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={this.closeModal}>
                        Close
                    </Button>
                    <Button
                        variant="primary"
                        onClick={() => this.apply(detailRequest)}>

                        Apply
                    </Button>
                </Modal.Footer>
            </Modal>
        } else {
            modal = undefined
        }

        return (
            <>
                <Table striped bordered hover size="sm">
                    <thead>
                    <tr>
                        <th>type</th>
                        <th>status</th>
                        <th>startDate</th>
                    </tr>
                    </thead>
                    <tbody>
                    {careRequestRows}
                    </tbody>
                </Table>
                {modal}

            </>
        )
    }
}