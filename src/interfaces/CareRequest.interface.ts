

export interface CareRequest {
    uuid: string; // should be type UUID, was unable to find a quick fix
    clientName: string;
    startDateTime: Date;
    endDateTime: Date;
    description: string;
    type: 'household' | 'medical'
    state: 'open' | 'closed'
}