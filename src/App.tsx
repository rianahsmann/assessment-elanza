import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Marketplace} from "./components/Marketplace";

const App: React.FC = () => {


    return (
        <Marketplace></Marketplace>
    );
}

export default App;
